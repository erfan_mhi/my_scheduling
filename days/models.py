from django.db import models
  
# Create your models here.



class Year(models.Model):
  goals = models.TextField(default='')
  lessons = models.TextField(default='')
  year = models.DateField(unique=True,null=True)

  def __str__(self):
    return "{}".format(str(self.year))

class Month(models.Model):
  goals = models.TextField(default='')
  month = models.DateField(unique=True)
  lessons = models.TextField(default='')
  year = models.ForeignKey(Year, on_delete=models.CASCADE)

  def __str__(self):
    return "{}".format(str(self.month))

class Week(models.Model):
  goals = models.TextField(default='')
  start_date = models.DateField(unique=True)
  end_date = models.DateField(unique=True)
  lessons = models.TextField(default='')
  month = models.ForeignKey(Month, on_delete=models.CASCADE)

  def __str__(self):
    return "{} to {}".format(str(self.start_date), str(self.end_date))

class Day(models.Model):
  goal = models.CharField(max_length=200)
  date = models.DateField(unique=True)
  week = models.ForeignKey(Week, on_delete=models.CASCADE)

  def __str__(self):
    return str(self.date)

class Task(models.Model):
  name = models.CharField(max_length=200)

  def __str__(self):
    return self.name

class TimeBox(models.Model):
  STATUS = (
        ('d', 'Done'),
        ('p', 'Partially Done'),
        ('n', 'Not Done Yet'),
        ('l', 'Lost'),
    )

  start_time = models.TimeField()
  finish_time = models.TimeField()
  day = models.ForeignKey(Day, on_delete=models.CASCADE)
  description = models.TextField(default='')
  task = models.ForeignKey(Task, on_delete=models.CASCADE)
  status = models.CharField(max_length=1, choices=STATUS)

  def __str__(self):
    return "{}: Date({}) {} to {}".format(self.task.name, str(self.day.date), str(self.start_time), str(self.finish_time))