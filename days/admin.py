from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Day)
admin.site.register(TimeBox)
admin.site.register(Task)
admin.site.register(Week)
admin.site.register(Month)
admin.site.register(Year)